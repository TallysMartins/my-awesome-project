# Android, adb and stuff

```adb shell pm list packages```

**Remove apk**

 ```adb uninstall package```

 ```pm uninstall package```

 ```adb shell am start -a android.intent.action.UNINSTALL_PACKAGE -d "package:PACKAGE"```

 ```adb shell am start -n com.android.packageinstaller/.UninstallerActivity -d "package:PACKAGE"```

 ```adb shell am start -a android.intent.action.DELETE -d "package:mypackage"```



**Also, to Remove APK remove /data/data/myapk and /system/app/myapk but before removing you need access to the fs:**

 ```su && mount -o rw,remount /system```

# General

**Remap keyboard keys**

xmodmap -e "keycode 105 = KP_Up"
xmodmap -e "keycode 91 = KP_Down"
xmodmap -e "keycode 108 = Left"

Applications

- VLC media player
- Darktable
- xPad
- JabRef
- gcolor2
- htop
- gimp
