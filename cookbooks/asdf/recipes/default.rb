packages = %W(automake autoconf libreadline-dev libncurses-dev libssl-dev 
          libyaml-dev libxslt-dev libffi-dev libtool unixodbc-dev unzip make)

packages.each do |pkg|
  package pkg
end

directory "/home/#{node['user']}/.asdf/" do
  owner node['user']
  group node['user']
  mode '0755'
  action :create
end

git "/home/#{node['user']}/.asdf/" do
  repository "https://github.com/asdf-vm/asdf.git"
  revision "master"
  action :sync
  user node['user']
end

bash "setting up asdf" do
  user node['user']
  group node['user']
  cwd "/home/#{node['user']}"
  code <<-EOH
    echo -e '\n. /home/#{node['user']}/.asdf/asdf.sh' >> /home/#{node['user']}/.bashrc
    echo -e '\n. /home/#{node['user']}/.asdf/completions/asdf.bash' >> /home/#{node['user']}/.bashrc
  EOH
  not_if { ::File.exist?("/home/#{node['user']}/.asdf/asdf.sh") }
end

available_plugins = {
  elixir: { install: false,
            url: 'https://github.com/asdf-vm/asdf-elixir.git',
            version: '1.4.0',
            deps: []
  },
  erlang: { install: false,
            url: 'https://github.com/asdf-vm/asdf-elixir.git',
            version: '19.0',
            deps: %W(build-essential autoconf m4 libncurses5-dev libssh-dev
                    libwxgtk3.0-dev libgl1-mesa-dev libglu1-mesa-dev libpng3
                    unixodbc-dev)
  },
  ruby:   { install: false,
            url: 'https://github.com/asdf-vm/asdf-ruby.git',
            version: '2.3.3',
            deps: []
  }
}

available_plugins.each do |k,v|
  #package resource does not have guards `only_if` and `not_if`
  if(v[:install] == true)

    v[:deps].each do |dep|
      package dep
    end
  
    execute "install plugin asdf #{k}" do
      command "./bin/asdf plugin-add #{k} #{v[:url]} || true"
      cwd "/home/#{node['user']}/.asdf/"
      user node['user']
    end

    execute "install the language #{k}" do
      command "./bin/asdf install #{k} #{v[:version]}"
      cwd "/home/#{node['user']}/.asdf/"
      user node['user']
    end

    execute "setting global version for #{k}" do
      command "./bin/asdf global #{k} #{v[:version]}"
      cwd "/home/#{node['user']}/.asdf/"
      environment "HOME" => "/home/#{node['user']}"
      user node['user']
    end
  end
end
