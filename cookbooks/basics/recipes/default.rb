package 'openssh-server'

ppas = { texstudio: 'ppa:blahota/texstudio' }


directory "/home/#{node['user']}/programs" do
  owner node['user']
  group node['user']
  mode '0775'
  action :create
end

############################
#USER PACKAGES
PKG_LIST = %w(
  xpad vlc openssh-client
  busybox curl wget silversearcher-ag lxc lxc-templates
  tmux htop gcolor2 texstudio evince redir
  tmate
)

PKG_LIST.each do |pkg|
  package pkg
end
#---------------------------------------------------------
