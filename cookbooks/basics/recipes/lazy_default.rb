#dev packages (tools)
#---------------------------------------------------------
pkg_list = %w(
 texlive-full texstudio
)

pkg_list.each do |pkg|
  package pkg
end
