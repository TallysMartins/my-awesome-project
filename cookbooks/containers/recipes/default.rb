PKG_LIST = %w(
  curl wget
  tmux htop
)

PKG_LIST.each do |pkg|
  package pkg
end
