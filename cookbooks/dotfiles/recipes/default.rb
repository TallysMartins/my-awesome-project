cookbook_file  "/home/#{node['user']}/.bashrc" do
  owner node['user'] 
  group node['user'] 
  mode  '0755'
end

cookbook_file  "/home/#{node['user']}/.bash_aliases" do
  owner node['user'] 
  group node['user'] 
  mode  '0755'
end

cookbook_file  "/home/#{node['user']}/.tmux.conf" do
  owner node['user'] 
  group node['user'] 
  mode  '0755'
end
