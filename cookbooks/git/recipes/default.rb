package 'git'

execute 'vim config user' do
  command 'git config --global user.name "Tallys Martins"'
  environment "HOME" => "/home/#{node['user']}"
  user node['user']
end

execute 'vim config user' do
  command 'git config --global user.email "tallysmartins@gmail.com"'
  environment "HOME" => "/home/#{node['user']}"
  user node['user']
end

execute 'vim config user' do
  command 'git config --global core.editor vim'
  environment "HOME" => "/home/#{node['user']}"
  user node['user']
end
