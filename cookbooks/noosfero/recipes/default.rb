noosfero_path = "/home/#{node['user']}/git/noosfero"
git noosfero_path do
  repository 'git@gitlab.com:noosfero/noosfero.git'
  revision 'master'
  action :sync
  user node['user']
end

dependencies = %w(
  locales lsb-release libxslt-dev libxml2-dev libmagickwand-dev nodejs
  libpq-dev imagemagick libmagickcore-dev libxslt-dev postgresql
  tango-icon-theme xvfb
)

dependencies.each do |dep|
  package dep
end

system = `echo $(lsb_release -sic) | awk '{print(tolower($1) "-" tolower($2))}'`.strip
file = "./script/install-dependencies/#{system}.sh"
execute 'create dependencies file' do
  command "touch #{file}"
  cwd noosfero_path
  user node['user']
end

execute 'Bundle Install' do
  command "bundle install"
  cwd noosfero_path
  user node['user']
end

execute 'noosfero quickstart' do
  cwd noosfero_path
  command "./script/quick-start"
  user node['user']
end

remotes = {
            meu: 'git@gitlab.com:TallysMartins/noosfero.git',
            fga: 'git@gitlab.com:unb-gama/noosfero.git',
            hubgit: 'git@github.com:tallysmartins/noosfero.git'
          }

remotes.each do |k, v|
  execute "adding noosfero remote #{k}" do
    cwd "/home/#{node['user']}/git/noosfero"
    command "git remote add #{k} #{v}"
    user node['user']
  end
end
