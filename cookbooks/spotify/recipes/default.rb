execute 'add key for spotify' do
  command 'sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886'
end

execute 'spotify source list' do
  command 'echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list'
end

execute 'update apt repo' do
  command 'sudo apt-get update'
end

package 'spotify-client'
