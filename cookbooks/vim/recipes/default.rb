package 'vim-athena'

directory "/home/#{node['user']}/git/" do
  owner node['user']
  group node['user']
  mode '0755'
  action :create
end

git "/home/#{node['user']}/git/lappis_vimrc" do
  repository 'https://github.com/tallysmartins/lappis_vimrc.git'
  revision 'master'
  action :sync
  user node['user']
end

execute 'install vimrc script' do
  cwd "/home/#{node['user']}/git/lappis_vimrc"
  command 'sh ./install.sh'
  user node['user']
end


#create directory for vim plugins
directory "/home/#{node['user']}/.vim/bundle" do
  owner node['user']
  group node['user']
  mode '0755'
  action :create
  recursive true
end

git 'install NerdTree' do
  repository 'https://github.com/scrooloose/nerdtree.git'
  destination "/home/#{node['user']}/.vim/bundle/nerdtree"
  revision 'master'
  action :sync
  user node['user']
end

git 'install colorscheme' do
  repository 'https://github.com/flazz/vim-colorschemes.git'
  destination "/home/#{node['user']}/.vim/bundle/colorschemes"
  revision 'master'
  action :sync
  user node['user']
end

git 'install ctrlP plugin' do
  repository 'https://github.com/ctrlpvim/ctrlp.vim.git'
  destination "/home/#{node['user']}/.vim/bundle/ctrlp.vims"
  revision 'master'
  action :sync
  user node['user']
end

